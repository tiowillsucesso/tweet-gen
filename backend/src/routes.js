const express = require('express');

const routes = express.Router();

const TweetController = require('../src/controllers/TweetController');
const LikeController = require('./controllers/LikeController');
const LoginController = require('./controllers/LoginController');
const ComentarioController = require('./controllers/CometarioController');

routes.post('/login', LoginController.logar);
routes.post('/cadastrar', LoginController.cadastrar);

routes.get('/tweets', TweetController.buscarTodos);
routes.post('/tweet', TweetController.salvar);

routes.post('/like/:id', LikeController.curtir);

routes.delete('/tweet/:id', TweetController.apagar);

routes.post('/comentar', ComentarioController.comentar);
routes.post('/descomentar', ComentarioController.descomentar);
routes.post('/curtirComentario', ComentarioController.curtir);

module.exports = routes;