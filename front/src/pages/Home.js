import React, { Component } from 'react';
import socket from 'socket.io-client';

import Logo from '../images/twiteer.png';
import api from '../services/api';
import Tweet from '../components/tweetComponent';
import Lateral from '../components/lateralComponent';

export default class Home extends Component {
  state = {
    tweets: [],
    newTweet: ''
  }

  async componentDidMount() {
    const autor = localStorage.getItem('@GoLuck:userName');

    if (!autor) {
      this.props.history.push('/');
    }
    this.atualizarTela();
    const resultado = await api.get('/tweets');

    this.setState({ tweets: resultado.data });
   
  }

  changeText = (e) => {
    this.setState({ newTweet: e.target.value });
  }

  enviarTweet = async e => {
    if (e.keyCode !== 13) return;

    const conteudo = this.state.newTweet;
    const autor = localStorage.getItem('@GoLuck:userName');

    await api.post('/tweet', { conteudo, autor });

    this.setState({ newTweet: '' });
  }

  atualizarTela = () => {
    const io = socket('http://localhost:3001');

    io.on('tweet', data => {
      this.setState({ tweets: [ data, ...this.state.tweets ] });
    });

    io.on('like', data => {
      this.setState({ tweets: this.state.tweets.map(tweet => 
        tweet._id === data._id ? data : tweet
      )});
    });

    io.on('apagar', data => {
      this.setState({ tweets: this.state.tweets.filter(tweet => tweet._id !== data._id)});
    });

    io.on('comentario', data => {
      this.setState({ tweets: this.state.tweets.map(tweet => (
        tweet._id === data._id ? data : tweet
      ))});
    })
     
  }

  render() {
    return (
      <div className="row justify-content-center home">
        <div className="col-3">{ <Lateral /> }</div>
        <div className="col-4 text-center">
          <img className="logo" src={Logo} alt="logo" />
          <div className="form-group">
            <textarea className="form-control"
                      placeholder="O que está acontecendo?"
                      onChange={this.changeText}
                      value={this.state.newTweet}
                      onKeyDown={this.enviarTweet}
                      /><br />
          </div>

          <ul className="tweetHome">
            { this.state.tweets.map(tweet => (
              <Tweet key={ tweet._id } tweet={tweet}/>
            )) }
          </ul>
        </div>
        <div className="col-3"></div>
      </div>
    )
  }
}