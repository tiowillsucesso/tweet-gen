const Usuario = require('../models/Usuario');

module.exports = {

  logar (req, res) {

    const name = req.body.userName;
    const pass = req.body.pass;

    Usuario.findOne({ userName: name, pass: pass}, (err, user) => {
      
      if (err) {
        console.log(err);
        return res.status(500).json({ message: 'Erro ao acessar o servidor.' });
      }

      if (!user) {
        return res.status(404).json({ message: 'Usuário ou senha inválidos.' });
      }

      return res.status(200).json(user);
    });        
  },

  async cadastrar (req, res) {
    
    const user = await Usuario.create(req.body);

    return res.json(user);
  }
}
