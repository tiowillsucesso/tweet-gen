import React, { Component } from 'react';

import Comentario from '../components/ComentarioComponent';

import api from '../services/api';
import './tweet.css';

export default class TweetComponent extends Component {

  state = {
    comentario: ''
  }

  curtir = async () => {
    const { _id } = this.props.tweet;

    await api.post(`/like/${_id}`);
  }

  apagar = async () => {
    const { _id } = this.props.tweet;

    await api.delete(`/tweet/${_id}`);
  }

  setarComentario = (e) => {
    this.setState({ comentario: e.target.value});
  }

  comentar = async (e) => {
    if (e.keyCode !== 13) return;

    const { _id } = this.props.tweet;
    const { autor } = this.props.tweet;

    await api.post('/comentar', { id: _id, autor: autor, conteudo: this.state.comentario});

    this.setState({ comentario: ''});
  }

  render() {
    const { tweet } = this.props;
    return (
      <li className="tweet">
        <strong>{ tweet.autor }</strong>
        <button type="button"
                className="btn btn-link apagarTweet"
                onClick={ this.apagar }>
          Excluir          
        </button>
        <p>{ tweet.conteudo }</p>
        <span>
          <button type="button"
                  className="btn btn-link"
                  onClick={ this.curtir }>
            <i className="far fa-heart"></i>          
          </button>
          { tweet.likes }
        </span><br />
        <input className="form-control"
               placeholder="comentar"
               value={ this.state.comentario }
               onChange={ this.setarComentario }
               onKeyDown={ this.comentar }/>
        <ul className="tweetHome">
          {tweet.comentarios.map(coment => (
            <Comentario key={ coment.data } comentario={ coment } />
          ))}
        </ul>
        <hr />
      </li>
    )
  }
}