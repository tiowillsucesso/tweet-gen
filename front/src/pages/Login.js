import React, { Component } from 'react';

import './estilo.css';
import Logo from '../images/twiteer.png';
import api from '../services/api';

export default class Login extends Component {

	state = {
    userName: '',
    pass: ''
	}

	changeName = e => {
		this.setState({ userName: e.target.value });
  }
  
  changePass = e => {
		this.setState({ pass: e.target.value });
	}

	logar = async e => {
    e.preventDefault();

		const { userName } = this.state;
    const { pass } = this.state;

		if (!userName.trim().length || !pass.trim().length) {
      alert('Usuário e senha obrigatórios');
			return;
    }

    await api.post('/login', { userName, pass}).then((result) => {

      localStorage.setItem('@GoLuck:userName', result.data.userName);
      this.props.history.push('/home');

    }, erro => {
      alert(erro.response.data.message);
      
      return;
    });    
	}

	render() {
		return (
			<div className="row justify-content-center pageLogin">
				<div className="col-3 text-center">
					<img className="logo" src={Logo} alt="logo" />
					<form onSubmit= {this.logar}>
						<input className="form-control" onChange={this.changeName} /><br />
            <input className="form-control" onChange={this.changePass} /> <br />
						<button className="btn btn-info tamTotal">Entrar</button>
					</form>
				</div>
			</div>
		)
	}
}
