const Tweet = require('../models/Tweet');

module.exports = {
    async buscarTodos(req, res) {
      const tweets = await Tweet.find({}).sort('-criadoEm');

      return res.json(tweets);
    },

    async salvar(req, res) {
      const tweet = await Tweet.create(req.body);

      req.io.emit('tweet', tweet);
      
      return res.json(tweet);
    },

    async apagar(req, res) {
      const tweet = await Tweet.findByIdAndDelete( req.params.id );

      req.io.emit('apagar', tweet);
      
      return res.json(tweet);
    }
}