import React, { Component } from "react";

import api from '../services/api';

export default class Comentario extends Component {

  curtirComentario = async () => { 
    const { comentario } = this.props;

    await api.post('/curtirComentario', { comentario: comentario });
  }

  apagarComentario = async () => {
    const { comentario } = this.props;

    await api.post('/descomentar', { comentario: comentario });
  }

  render() {
    const { comentario } = this.props;
    return (
      <li>
        <span>{comentario.conteudo}</span>
        <span>
          <button type="button"
            className="btn btn-link"
            onClick={this.curtirComentario}>
            <i className="far fa-heart"></i>
          </button>
          {comentario.likes}
        </span>
        <button type="button"
          className="btn btn-link apagarTweet"
          onClick={this.apagarComentario}>
          <i className="far fa-trash-alt"></i>
        </button>
      </li>
    )
  }
}

