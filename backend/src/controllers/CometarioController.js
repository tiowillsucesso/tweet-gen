const Tweet      = require('../models/Tweet');

module.exports = {
  async curtir(req, res) {
    const coment = req.body.comentario;
    const tweet = await Tweet.findById(coment.idTweet);

    for (let i = 0; i < tweet.comentarios.length; i++) {
      if (tweet.comentarios[i].idTweet === coment.idTweet) {
        tweet.comentarios[i] = ({
          idTweet: tweet.comentarios[i].idTweet,
          autor: tweet.comentarios[i].autor,
          conteudo: tweet.comentarios[i].conteudo,
          data: tweet.comentarios[i].data,
          comLikes: tweet.comentarios[i].likes + 1
        });
      }
    }
    // tweet.comentarios.map(data => (
    //   data.data === coment.data && data.autor === coment.autor ?
    //     data.set({ comLikes: comLikes + 1 }) : ''
    // ));

    await tweet.save();
    req.io.emit('comentario', tweet);

    return res.json(tweet);
  },

  async comentar(req, res) {
    const tweet = await Tweet.findById(req.body.id);

    const comentario = ({
      idTweet: req.body.id,
      autor: req.body.autor,
      conteudo: req.body.conteudo,
      data: new Date(),
      comLikes: 0
    });

    tweet.set({ comentarios: [...tweet.comentarios, comentario]});
    await tweet.save();

    req.io.emit('comentario', tweet);
    return res.json(tweet);
  },

  async descomentar(req, res) {
    const tweet = await Tweet.findById(req.body.id);

    const coment = req.body.comentario;

    tweet.set({ comentarios: tweet.comentarios.filter(data => 
      data.data !== coment.data && data.autor !== coment.autor
    )});

    await tweet.save();
    req.io.emit('comentario', tweet);

    return res.json(tweet);
  }
}